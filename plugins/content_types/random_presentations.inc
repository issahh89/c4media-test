<?php

$plugin = array(
  'single' => TRUE,
  'title' => t('Random Presentations'),
  'description' => t('Lists 3 random presentations in a table.'),
  'category' => t('C4 Media'),
  'render callback' => 'c4media_test_random_presentations_render',
  'admin info' => 'c4media_test_random_presentations_admin_info',
);

?>