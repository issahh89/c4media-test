<?php
/**
 * @file
 * c4media_test.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function c4media_test_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'presentations';
  $page->task = 'page';
  $page->admin_title = 'Presentations';
  $page->admin_description = '';
  $page->path = 'c4media_test';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_presentations__panel';
  $handler->task = 'page';
  $handler->subtask = 'presentations';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Presentations';
  $display->uuid = 'b6138db6-80fe-48a6-9758-08702e2592a8';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_presentations__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-15ab797f-997e-4168-9363-1c1e0175a2b1';
  $pane->panel = 'center';
  $pane->type = 'block';
  $pane->subtype = 'views-presentations-top';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '15ab797f-997e-4168-9363-1c1e0175a2b1';
  $display->content['new-15ab797f-997e-4168-9363-1c1e0175a2b1'] = $pane;
  $display->panels['center'][0] = 'new-15ab797f-997e-4168-9363-1c1e0175a2b1';
  $pane = new stdClass();
  $pane->pid = 'new-172a5ab8-18e0-4f3f-ab4b-985e058c3996';
  $pane->panel = 'center';
  $pane->type = 'random_presentations';
  $pane->subtype = 'random_presentations';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '172a5ab8-18e0-4f3f-ab4b-985e058c3996';
  $display->content['new-172a5ab8-18e0-4f3f-ab4b-985e058c3996'] = $pane;
  $display->panels['center'][1] = 'new-172a5ab8-18e0-4f3f-ab4b-985e058c3996';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['presentations'] = $page;

  return $pages;

}
